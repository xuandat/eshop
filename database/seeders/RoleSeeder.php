<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = config('policies.roles');

        DB::table('roles')->delete();
        DB::table('roles')->insert($roles);

        $resources = config('policies.resources');
        $actions = config('policies.policy_actions');

        /**
         * permissions table
         */
        $id = 1;
        DB::table('permissions')->delete();
        $permissions = [];

        foreach ($resources as $resource) {
            $id_parent =  $id++;

            $permission_parent['id'] =  $id_parent;
            $permission_parent['name'] =   $resource['name'];
            $permission_parent['parent_id'] = 0;
            $permission_parent['description'] = $resource['display_name'];
            $permissions[] = $permission_parent;

            foreach ($actions as $action) {
                $permission['id'] = $id;
                $permission['name'] = $resource['name'] . '_' . $action['name'];
                $permission['parent_id'] = $id_parent;
                $permission['description'] = $action['display_name'] . ' ' . $resource['display_name'];
                $permissions[] = $permission;
                ++$id;
            }
        }
        DB::table('permissions')->insert($permissions);

        /**
         * role_user table
         */
        DB::table('role_user')->delete();
        $role_user = config('policies.role_user');
        DB::table('role_user')->insert($role_user);

        /**
         * permission_role table
         */
        DB::table('permission_role')->delete();
        $permission_role = config('policies.permission_role');
        DB::table('permission_role')->insert($permission_role);
    }
}
