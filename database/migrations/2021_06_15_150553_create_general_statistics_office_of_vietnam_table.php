<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralStatisticsOfficeOfVietnamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gso_provinces', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('province');
        });

        Schema::create('gso_districts', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('district');
            $table->bigInteger('province_id');
            $table->string('province');
        });

        Schema::create('gso_wards', function (Blueprint $table) {
            $table->string('id');
            $table->string('ward');
            $table->bigInteger('district_id');
            $table->string('district');
            $table->bigInteger('province_id');
            $table->string('province');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gso_provinces');
        Schema::dropIfExists('gso_districts');
        Schema::dropIfExists('gso_wards');
    }
}
