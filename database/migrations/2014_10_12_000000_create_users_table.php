<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->string('profile_photo_path', 2048)->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('address')->nullable(); // Địa chỉ chi tiết, thôn/ấp/xóm/ building,..
            $table->string('ward_id')->nullable(); // Tên phường/xã - Tên đường/phố
            $table->string('district_id')->nullable(); //Tên quận/huyện
            $table->string('province_id')->nullable(); //Tên tỉnh/thành phố
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
