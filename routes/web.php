<?php

use App\Http\Controllers\Admins\UserController;
use App\Http\Controllers\Admins\OrderController;
use App\Http\Controllers\Admins\ProductController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\GSOController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('eshop.pages.index');
});

Route::group([
    'prefix' => 'admin',
    'middleware' => [
        'auth',
    ],
], function () {
    Route::get('/', function () {
        return view('admin.dashboard');
    });

    Route::apiResource('products', ProductController::class);
    Route::get('products-list', [ProductController::class, 'list'])->name('product.list');

    Route::apiResource('customers', UserController::class);

    Route::get('customers-list', [UserController::class, 'list'])->name('customer.list');

    Route::apiResource('permission', PermissionController::class)->middleware('isAdmin');

    Route::apiResource('roles', RoleController::class)->middleware('isAdmin');
});

Route::group([
    'prefix' => 'gso',
], function () {
    Route::get('provinces', [GSOController::class, 'indexProvince']);
    Route::get('districts/{province_id}', [GSOController::class, 'indexDistricts']);
    Route::get('wards/{district_id}', [GSOController::class, 'indexWards']);
});

Route::group([
    'prefix' => 'account',
], function () {
    Route::get('/register', [UserController::class, 'register']);
    Route::post('/register', [UserController::class, 'handleRegister']);
    Route::get('/login', [UserController::class, 'login']);
});

Auth::routes();
