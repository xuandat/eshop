@extends('adminlte::page')

@section('title', 'Products')

@section('content_header')
    <h1>Products</h1>
@stop

@section('content')
    <p>Welcome to this beautiful admin panel.</p>

    <div id="product">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-9 mt-2">
                        <h3 class="card-title ">DataTable with default features</h3>
                        <input class="form-control txt-live_search" type="text" placeholder="Search..">
                    </div>
                    <div class="col-sm-3 float-right">
                        <button class="btn btn-success float-right ml-2 mr-2 mt-4 btn-create">Create New
                        </button>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="tblProduct" class="table table-bordered table-striped btn-new btn-new-product">
                    <thead>
                        <th field="name">Name</th>
                        <th field="price">Price</th>
                        <th field="quantity">Quantity</th>
                        <th field="images">Image</th>
                        <th field="status">status</th>
                        <th width="150" class="text-center">Action</th>
                    </thead>
                    <tbody class="product-items">
                    </tbody>
                </table>
                <div id="paginate-product"></div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <!-- The Modal -->
        <div class="modal" id="modalProduct">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">New Product</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form id="frmProduct" name="frmProduct" role="form"
                        method="post" enctype="multipart/form-data"
                        >
                            <input name="productId" type="hidden">
                            <div class="row">
                                <div class="col-sm-7">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Enter ...">
                                        <small id="nameHelper" class="text-danger"></small>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Code</label>
                                        <input type="text" name="code" class="form-control" placeholder="Enter ...">
                                        <small id="codeHelper" class="text-danger"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input type="text" name="price" class="form-control" placeholder="Enter ...">
                                        <small id="priceHelper" class="text-danger"></small>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Weight</label>
                                        <input type="text" name="weight" class="form-control" placeholder="Enter ...">
                                        <small id="weightHelper" class="text-danger"></small>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <input type="text" name="quantity" class="form-control" placeholder="Enter ...">
                                        <small id="quantityHelper" class="text-danger"></small>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <!-- select -->
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="0">Enable</option>
                                            <option value="1">Disable</option>
                                            <option value="2">Sale Off</option>
                                        </select>
                                        <small id="statusHelper" class="text-danger"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- textarea -->
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description" rows="3"
                                            placeholder="Enter ..."></textarea>
                                        <small id="descriptionHelper" class="text-danger"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Images</label>
                                    <input type="file" name="cover_image" id="cover_image">
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success btn-save">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
