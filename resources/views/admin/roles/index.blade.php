@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
    <h1>Roles</h1>
@stop

@section('content')
    <p>Welcome to this beautiful admin panel.</p>

    <div id="role">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-9 mt-2">
                        <h3 class="card-title ">DataTable with default features</h3>
                        <input class="form-control txt-live_search" type="text" placeholder="Search..">
                    </div>
                    <div class="col-sm-3 float-right">
                        <button class="btn btn-success float-right ml-2 mr-2 mt-4 btn-create">Create New
                        </button>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="tblRole" class="table table-bordered table-striped btn-new btn-new-product">
                    <thead>
                        <th field="index">#</th>
                        <th field="role">Role</th>
                        <th field="description">Description</th>
                        <th width="150" class="text-center">Action</th>
                    </thead>
                    <tbody class="product-items">
                        @foreach ($roles as $role)
                            <tr data-id={{ $role->id }}>
                                <td>{{ $loop->index }} </td>
                                <td>{{ $role->name }} </td>
                                <td>{{ $role->description }} </td>
                                <td>
                                    <button type="button" class="btn btn-success btn-sm btn-edit  ">Edit </button>
                                    {{-- <button type="button" class="btn btn-danger  btn-sm btn-delete">Delete</button> --}}
                                </td>
                            </tr>
                        @endforeach

                    </tbody>

                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <div id="role-paginate">
                    {{ $roles->links() }}
                </div>
            </div>
        </div>
        <!-- /.card -->
        @include('admin.roles.create-edit')
    </div>
@stop
