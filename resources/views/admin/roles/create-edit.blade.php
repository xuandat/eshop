<!-- Modal -->
<div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formCreateRole" name="formCreateRole" role="form" method="post" action="">
                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="role_name">Role Name</label><br>
                                    <input type="text" name="name" id="role_name" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="role_description">Description</label><br>
                                    <textarea name="description" id="role_description" cols="30" rows="6"
                                        class="form-control"></textarea>
                                </div>
                            </div>

                            @foreach ($permission_parents as $permissionParentItem)
                                <div class="card border-primary mb-3 col-md-12">
                                    <div class="card-header">
                                        <label style="cursor: pointer">
                                            <input type="checkbox" value="{{ $permissionParentItem['name'] }}"
                                                class="checkbox_wrapper permission__module">
                                            Module {{ $permissionParentItem['description'] }}
                                        </label>
                                    </div>
                                    <div class="row">

                                        @foreach ($permissionParentItem->permissions as $permission)
                                            <div class="card-body text-primary col-md-3">
                                                <h5 class="card-title">
                                                    <label style="cursor: pointer">
                                                        <input type="checkbox"
                                                            name="permission_id[]" class="checkbox_childrent"
                                                            id="permission_{{$permission['name']}}"
                                                            value="{{ $permission['id'] }}">

                                                        {{ $permission['description'] }}
                                                    </label>
                                                </h5>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
