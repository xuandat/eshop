<!-- The Modal -->
<div class="modal" id="modalCustomer">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">New Product</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form id="formCustomer" name="formCustomer" role="form">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">First Name</label>
                            <input type="name" class="form-control" id="name" name="name"
                                placeholder="First Name">
                            <small id="nameHelper" class="text-danger"></small>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="last_name">Last Name</label>
                            <input type="last_name" class="form-control" id="last_name" name="last_name"
                                placeholder="Last Name">
                            <small id="last_nameHelper" class="text-danger"></small>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            <small id="emailHelper" class="text-danger"></small>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                                placeholder="Password">
                            <small id="passwordHelper" class="text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="+84346479339">
                        <small id="phoneHelper" class="text-danger"></small>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="province">Province</label>
                            <select id="province" name="province" class="form-control">
                            </select>
                            <small id="provinceHelper" class="text-danger"></small>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="district">District</label>
                            <select id="district" name="district" class="form-control">
                            </select>
                            <small id="districtHelper" class="text-danger"></small>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="ward_street">Ward Street</label>
                            <select id="ward_street" name="ward_street" class="form-control">
                            </select>
                            <small id="ward_streetHelper" class="text-danger"></small>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St">
                        <small id="addressHelper" class="text-danger"></small>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success btn-save" id="btnSaveCustomer">Save</button>
            </div>

        </div>
    </div>
</div>
