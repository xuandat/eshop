@extends('adminlte::page')

@section('title', 'Customers')

@section('content_header')
    <h1>Customers</h1>
@stop

@section('content')
    <p>Welcome to this beautiful admin panel.</p>

    <div id="customer">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-9 mt-2">
                        <h3 class="card-title ">DataTable with default features</h3>
                        <input class="form-control txt-live_search" type="text" placeholder="Search..">
                    </div>
                    <div class="col-sm-3 float-right mt-3">
                        <button class="btn btn-success float-right ml-2 mr-2 mt-2 btn-create">Create
                            New</button>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered datatable" id="tblCustomer">
                        <thead>
                            <tr>
                                <th field='id'>Id</th>
                                <th field='name'>Name</th>
                                <th field='phone'>Phone</th>
                                <th field='email'>Email</th>
                                <th field='address'>Address</th>
                                <th width="150" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div id="paginate-customer"></div>
                </div>
            </div>
        </div>

        @include('admin.customers.modal_create-edit')
    </div>
@stop

@section('js')
@stop

@section('css')
@stop
