import { dvhc, elSelectOptionDVHC as htmlDVHC } from "./don-vi-hanh-chinh";

class UI {
    fire(response) {
        let icon_status = response.status === "success" ? "success" : "error";
        let title_message = response.message;

        Swal.fire({
            position: "top-end",
            icon: icon_status,
            title: title_message,
            showConfirmButton: false,
            timer: 1500
        });
    }

    /**
     * Show Alert Message Error
     * @param response
     */
    fail(response) {
        var msg = response;

        if (response.statusText) {
            msg = response.statusText;
        }

        Swal.fire({
            position: "top-end",
            icon: "error",
            title: msg,
            showConfirmButton: false,
            timer: 1500
        });
    }

    confirmDelete() {
        return Swal.fire({
            title: "Bạn chắc chưa?",
            text: "Bạn sẽ không thể phục hồi",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Xóa"
        });
    }

    done(response) {
        var msg = response;

        if (response.message) {
            msg = response.message;
        }

        Swal.fire({
            position: "top-end",
            icon: "success",
            title: msg,
            showConfirmButton: false,
            timer: 1500
        });
    }

    paginate(links, position = "#paginate-product") {
        var ul = '<ul class="pagination mt-4 justify-content-end">';

        $.each(links, function(index, li) {
            if (li["url"]) {
                ul += `<li class="page-item ${li.active ? "active" : ""}">
                    <a class="page-link" href="#" data-page="${li["url"]}">
                        ${li["label"]}
                    </a>
                </li>`;
            }
        });

        ul += "</ul>";
        $(position).html(ul);
    }
}

let ui = new UI();

class Form {
    constructor(elmentById, base_action_url) {
        this.form = $(elmentById);
        this.base_action_url = base_action_url;
    }

    bindDataForUpdate(resource) {
        let form = this.form;

        $.each(resource, function(key, value) {
            if (key !== "password") {
                form.find("[name='" + key + "']").val(value);
            }
        });

        form.attr("method", "PATCH");
        form.attr("action", this.base_action_url + "/" + resource.id);

        if (form.find(`[name="province"]`).length === 1) {
            this.setHtmlDVHC(
                resource.province,
                resource.district,
                resource.ward_street
            );
        }
    }

    /**
     * Gan Du Lieu Don Vi Hanh Chinh Cac Tinh Thanh Vao Element Seleect Option
     */
    setHtmlDVHC(province_id, district_id, ward_street_id) {
        let form = this.form;

        let el_provice = form.find(`[name="province"]`);
        let el_district = form.find(`[name="district"]`);
        let el_ward_street = form.find(`[name="ward_street"]`);

        el_provice.html(dvhc.provinces()).val(province_id);

        el_district.html(dvhc.districts(province_id)).val(district_id);

        el_ward_street.html(dvhc.ward_street(district_id)).val(ward_street_id);

        htmlDVHC.change();
    }

    showErrorsHelper(errors) {
        let error,
            form = this.form;

        form.find("small").text("");
        for (error in errors) {
            form.find(`small[id="${error}Helper"]`).text(errors[error][0]);
        }
    }

    trigger(event) {
        this.form.trigger(event);

        if (event === "reset") {
            this.form.attr("method", "POST");
            this.form.attr("action", this.base_action_url);
            htmlDVHC.reset();
        }
    }

    setPost(){
        this.form.attr("method", "POST");
        this.form.attr("action", this.base_action_url);
    }

    submit() {
        return $.ajax({
            url: this.form.attr("action"),
            type: this.form.attr("method"),
            data: this.form.serialize(),
            dataType: "JSON"
        });
    }
}

class Table {
    autoBinding(data, tableId) {
        $(tableId + " tbody").text("");
        let ths = $(tableId + " thead th[field]");

        $.each(data, function(index, item) {
            let tr = $(`<tr></tr>`);

            $.each(ths, function(index, th) {
                let field = $(th).attr("field");
                let value = item[field];
                let td = $(`<td>${value}</td>`);

                if (field === "images") {
                    td = $(
                        `<td><img alt="" src="/storage/images/${value}" width="50"></td>`
                    );
                }

                tr.append(td);
            });

            let actions = $(`<td>
                <button type="button" class="btn btn-success btn-sm btn-edit  " data-id="${item["id"]}">Edit  </button>
                <button type="button" class="btn btn-danger  btn-sm btn-delete" data-id="${item["id"]}">Delete</button>
            </td>`);

            tr.append(actions);

            $(tableId + " tbody").append(tr);
        });
    }
}
const table = new Table();

/**
 * Modal Bootstrap 4 Component
 */
var modalView = {
    modal: $("#modal"),

    set(elementByID = "#modal") {
        this.modal = $(elementByID);
    },

    show(title = "My Modal") {
        if (title) {
            this.modal.find(".modal-title").text(title);
        }
        this.modal.modal("show");
    },

    hide() {
        this.modal.modal("hide");
    }
};

export { ui, Form, table, modalView as modal };
