class DB {
    constructor(_base_url) {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });

        this.base_url = _base_url;
    }

    /**
     *
     * @param {string} id - id resource
     * @param {object} form - element object
     * @returns
     */
    serializing(form, id = null) {
        if (id) {
            this.base_url += "/" + id;
        }

        return $.ajax({
            url: this.base_url,
            method: "post",
            data: form.serialize(),
            dataType: "JSON"
        });
    }

    get(id) {
        var dynamicData = {};
        dynamicData["id"] = id;
        return $.ajax({
            url: this.base_url + "/" + id,
            type: "get",
            data: dynamicData
        });
    }

    destroy(id) {
        return $.ajax({
            url: this.base_url + "/" + id,
            type: "DELETE",
            dataType: "json"
        });
    }

    getList(url = "list", key_word_live_search = "", page = 1) {
        return $.ajax({
            url: `${this.base_url}-${url}`,
            type: "GET",
            data: {
                key_word_live_search: key_word_live_search,
                page: page
            }
        });
    }

    submitWithFile(formDataElementById) {
        let form = $(`#${formDataElementById}`);
        // let formData = new FormData(form);

        let myForm = document.getElementById(formDataElementById);
        let formData = new FormData(myForm);

        return $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            contentType: false,
            processData: false
        });
    }
}

export { DB };
