// IIFE - Immediately Invoked Function Expression

import { ui, Form, modal } from "../core/ui";
import { DB } from "../core/db";
import { forEach } from "lodash";

(function($, window, document) {
    // The $ is now locally scoped
    class Role extends DB {
        constructor(base_url) {
            super(base_url);

            this.form = new Form("#formCreateRole", base_url);
            modal.set("#roleModal");
        }

        bindDataForUpdate(response) {
            this.form.bindDataForUpdate(response.role);

            let permissionsChecked = response.permissionsChecked;
            $("input[type='checkbox']").prop("checked", false);

            permissionsChecked.forEach(element => {
                let item = $("#permission_" + element.name);
                item.prop("checked", true);
            });
        }
    }

    // Listen for the jQuery ready event on the document
    $(function() {
        // Event: Display Customer
        let roles = new Role("/admin/roles");
        let timeout = null;

        $(".checkbox_wrapper").on("click", function() {
            $(this)
                .parents(".card")
                .find(".checkbox_childrent")
                .prop("checked", $(this).prop("checked"));
        });

        $(".checkall").on("click", function() {
            $(this)
                .parents()
                .find(".checkbox_childrent")
                .prop("checked", $(this).prop("checked"));
            $(this)
                .parents()
                .find(".checkbox_wrapper")
                .prop("checked", $(this).prop("checked"));
        });

        $("#role")
            .on("click", ".btn-create", function() {
                console.log("hi");

                roles.form.setPost();
                modal.show("Create Role.");
            })
            .on("click", ".btn-edit", function() {
                let id = $(this)
                    .closest("tr")
                    .data("id");

                roles
                    .get(id)
                    .done(function(response) {
                        roles.bindDataForUpdate(response);
                        modal.show("Update Role.");
                    })
                    .fail(function(response) {});
            })
            .on("click", ".btn-save", function() {
                roles.form
                    .submit()
                    .done(function(res) {
                        console.log(res);

                        let timeout = 800;

                        ui.done(res.message);
                        modal.hide();

                        clearTimeout(timeout);
                        timeout = setTimeout(function() {
                            window.location.reload();
                        }, 800);
                    })
                    .fail(function(res) {
                        ui.fail(res.responseJSON.message);
                        model.form.showErrorsHelper(res.responseJSON.errors);

                        console.log(res);
                    });
            });
    });

    // The rest of the code goes here!
})(window.jQuery, window, document);
// The global jQuery object is passed as a parameter
