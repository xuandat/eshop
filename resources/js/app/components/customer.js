// IIFE - Immediately Invoked Function Expression

import { ui, Form, table, modal } from "../core/ui";
import { DB } from "../core/db";

(function ($, window, document) {
    // The $ is now locally scoped
    class Customers extends DB {
        constructor(base_url) {
            super(base_url);

            this.table = $("#tblCustomer");
            this.form = new Form("#formCustomer", base_url);
            modal.set("#modalCustomer");
            this.bindDataToTable();
        }

        /**
         * Ket gan dl len table
         */
        bindDataToTable(page = 1) {
            let key_word_search = $(".txt-live_search").val();

            this.getList("list", key_word_search, page)
                .done(function (res) {
                    table.autoBinding(res.data.data, "#tblCustomer");
                    ui.paginate(res.data.links, "#paginate-customer");
                })
                .fail(function (res) {
                    console.log(res.data.data);
                });
        }
    }

    // Listen for the jQuery ready event on the document
    $(function () {
        // Event: Display Customer
        let model = new Customers("/admin/customers");
        let timeout = null;

        $("#customer")
            .on("click", ".btn-create", function () {
                model.form.trigger("reset");
                modal.show("Create New Customer");
            })
            .on("click", ".btn-edit", function () {
                let id = $(this).data("id");
                model
                    .get(id)
                    .done(function (response) {
                        model.form.bindDataForUpdate(response.data);
                        modal.show("Update Customer");
                        model.bindDataToTable();
                    })
                    .fail(function (response) {
                        ui.fail(response);
                    });
            })
            .on("click", ".btn-save", function () {
                model.form
                    .submit()
                    .done(function (res) {
                        ui.done(res.message);
                        modal.hide();
                        model.bindDataToTable();
                    })
                    .fail(function (res) {
                        ui.fail(res.responseJSON.message);
                        model.form.showErrorsHelper(res.responseJSON.errors);

                        console.log(res);
                    });
            })
            .on("click", ".btn-delete", function () {
                ui.confirmDelete().then((result) => {
                    if (result.value) {
                        let id = $(this).data("id");

                        model
                            .destroy(id)
                            .done(function (res) {
                                model.bindDataToTable();
                                ui.done(res);
                            })
                            .fail(function (res) {
                                ui.fail(res);
                            });
                    }
                });
            })
            .on("keyup", ".txt-live_search", function () {
                let timeout = 800;
                clearTimeout(timeout);

                timeout = setTimeout(function () {
                    model.bindDataToTable();
                }, 800);
            })
            .on("click", ".page-link", function (event) {
                event.preventDefault();

                $("li.active").removeClass("active");
                $(this).parent().addClass("active");

                let page_link = $(".page-item.active .page-link").data("page");
                let page = 1;
                if (typeof page_link === "string") {
                    let page_active = page_link.search("page=");
                    page = page_link.substring(page_active + 5);
                    console.log(page);
                }

                model.bindDataToTable(page);
            });
    });

    // The rest of the code goes here!
})(window.jQuery, window, document);
// The global jQuery object is passed as a parameter
