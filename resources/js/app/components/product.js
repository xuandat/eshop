// IIFE - Immediately Invoked Function Expression

import { ui, Form, table } from "../core/ui";
import { DB } from "../core/db";
import { toInteger } from "lodash";

(function($, window, document) {
    // The $ is now locally scoped
    class Product extends DB {
        constructor(base_url) {
            super(base_url);

            this.form = new Form("#frmProduct", base_url);

            this.bindDataToTable();
        }

        bindDataToTable(page = 1) {
            let key_word_search = $(".txt-live_search").val();

            this.getList("list", key_word_search, page)
                .done(function(res) {
                    table.autoBinding(res.data.data, "#tblProduct");
                    ui.paginate(res.data.links, "#paginate-product");
                })
                .fail(function(res) {
                    console.log(res.data.data);
                });
        }

        modal(event, title = null) {
            var el = $("#modalProduct");

            if (title) {
                el.find(".modal-title").text(title);
            }

            el.modal(event);

            return this;
        }
    }

    // Listen for the jQuery ready event on the document
    $(function() {
        // Event: Display Customer
        let products = new Product("/admin/products");
        let timeout = null;

        $("#product")
            .on("click", ".btn-create", function() {
                products.form.trigger("reset");
                products.modal("show", "Create New Product");
            })
            .on("click", ".btn-edit", function() {
                let id = $(this).data("id");
                products
                    .get(id)
                    .done(function(response) {
                        products.form.bindDataForUpdate(response.data);
                        products.modal("show", "Update Product");
                    })
                    .fail(function(response) {
                        ui.fail(response);
                    });
            })
            .on("click", ".btn-save", function() {
                products
                    .submitWithFile("frmProduct")
                    .done(function(res) {
                        ui.done(res.message);
                        products.modal("hide").bindDataToTable();
                    })
                    .fail(function(res) {
                        console.log(res);
                        if (res.status === 403) {
                            ui.fail(res.responseJSON.message);
                        }
                    });
            })
            .on("click", ".btn-delete", function() {
                ui.confirmDelete().then(result => {
                    if (result.value) {
                        let id = $(this).data("id");

                        products
                            .destroy(id)
                            .done(function(res) {
                                products.bindDataToTable();
                                ui.done(res);
                            })
                            .fail(function(res) {
                                ui.fail(res);
                            });
                    }
                });
            })
            .on("keyup", ".txt-live_search", function() {
                let timeout = 800;
                clearTimeout(timeout);

                timeout = setTimeout(function() {
                    products.bindDataToTable();
                }, 800);
            })
            .on("click", ".page-link", function(event) {
                event.preventDefault();

                $("li.active").removeClass("active");
                $(this)
                    .parent()
                    .addClass("active");

                let page_link = $(".page-item.active .page-link").data("page");
                let page = 1;
                if (typeof page_link === "string") {
                    let page_active = page_link.search("page=");
                    page = page_link.substring(page_active + 5);
                    console.log(page);
                }

                products.bindDataToTable(page);
            });
    });

    // The rest of the code goes here!
})(window.jQuery, window, document);
// The global jQuery object is passed as a parameter
