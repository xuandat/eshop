<?php
return [
    'roles' => [
        ['id' => 1, 'name' => 'admin', 'description' => ''],
        ['id' => 2, 'name' => 'manager', 'description' => ''],
        ['id' => 3, 'name' => 'sales', 'description' => ''],
        ['id' => 4, 'name' => 'guest', 'description' => ''],
    ],

    'resources' => [
        [
            'id' => '1',
            'name' => 'product',
            'display_name' => 'Product',
            'description' => ''
        ],

        [
            'id' => '2',
            'name' => 'user',
            'display_name' => 'User',
            'description' => ''
        ],

        [
            'name' => 'role',
            'display_name' => 'Role',
            'description' => ''
        ],
    ],

    'policy_actions' => [
        [
            'name' => 'forceDelete',
            'display_name' => 'Delete'
        ],
        [
            'name' => 'restore',
            'display_name' => 'Restore'
        ],
        [
            'name' => 'delete',
            'display_name' => 'Delete'
        ],
        [
            'name' => 'update',
            'display_name' => 'Edit'
        ],
        [
            'name' => 'create',
            'display_name' => 'Create'
        ],
        [
            'name' => 'view',
            'display_name' => 'Read'
        ],
        [
            'name' => 'viewAny',
            'display_name' => 'View All'
        ]
    ],

    'permission_role' => [
        [
            'permission_id' => 1,
            'role_id' => 1,
        ],
        [
            'permission_id' => 1,
            'role_id' => 2,
        ],
    ],

    'permission_user' => [
        [
            'user_id' => '',
            'role_id' => '',
            'description' => '',
        ],
    ],

    'role_user' => [
        [
            'role_id' => 1,
            'user_id' => 1,
        ],
    ],
];
