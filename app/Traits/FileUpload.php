<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

trait FileUpload
{
    public function uploadFile(Request $request)
    {
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $filename = $this->slug($filename);

            // Get just Extension
            $extension = $request->file('cover_image')->getClientOriginalExtension();

            // Filename To store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;

            // Upload Image
            $path = $request->file('cover_image')->storeAs('images', $fileNameToStore);
        }
        // Else add a dummy image
        else {
            $fileNameToStore = 'no_images.jpg';
        }

        return $fileNameToStore;
    }

    private function slug($str){
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);

        return $str;
    }
}
