<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VietnamPhoneNumberRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $phone_header = '/(086|096|097|098|032|033|034|035|036|037|038|039,091|094|083|084|085|081|082|089|090|093|070|079|077|076|078|092|058|099|059|087)';
        return preg_match($phone_header . '+([0-9]{7})\b/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'So Phone Chua Chinh Xac';
    }
}
