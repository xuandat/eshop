<?php

namespace App\Providers;

use App\Repositories\Eloquents\BaseRepository;
use App\Repositories\Eloquents\ProductRepository;
use App\Repositories\Eloquents\UserRepository;
use App\Repositories\BaseRepositoryInterface;
use App\Repositories\Eloquents\GSO\GSODistrictRepository;
use App\Repositories\Eloquents\GSO\GSOProvinceRepository;
use App\Repositories\Eloquents\GSO\GSOWardRepository;
use App\Repositories\Eloquents\PermissionRepository;
use App\Repositories\Eloquents\RoleRepository;
use App\Repositories\GSODistrictContract;
use App\Repositories\GSOProvinceContract;
use App\Repositories\GSOWardContract;
use App\Repositories\PermissionRepositoryContract;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\RoleRepositoryContract;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);

        // GSO
        $this->app->bind(GSOProvinceContract::class, GSOProvinceRepository::class);
        $this->app->bind(GSODistrictContract::class, GSODistrictRepository::class);
        $this->app->bind(GSOWardContract::class, GSOWardRepository::class);

        /**
         * Roles
         */
        $this->app->bind(RoleRepositoryContract::class, RoleRepository::class);
        $this->app->bind(PermissionRepositoryContract::class, PermissionRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
