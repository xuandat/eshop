<?php

namespace App\Services\GSO;

use App\Repositories\GSOWardContract;
use function PHPUnit\Framework\returnArgument;

/**
 *
 */
class GSOWardService extends \App\Services\BaseService
{
    /**
     * @var GSOWardContract
     */
    protected $wardContract;

    /**
     * @param GSOWardContract $wardContract
     */
    public function __construct(GSOWardContract $wardContract)
    {
        $this->wardContract = $wardContract;
    }

    public function getListWardsInDistrict($district_id)
    {
        $wards = $this->wardContract->wardsInDistrict($district_id);

        return $wards;
    }
}
