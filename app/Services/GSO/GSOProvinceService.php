<?php

namespace App\Services\GSO;

use App\Repositories\GSOProvinceContract;
use function PHPUnit\Framework\returnArgument;

/**
 *
 */
class GSOProvinceService extends \App\Services\BaseService
{
    /**
     * @var GSOProvinceContract
     */
    protected $provinceContract;

    /**
     * @param GSOProvinceContract $provinceContract
     */
    public function __construct(GSOProvinceContract $provinceContract)
    {
        $this->provinceContract = $provinceContract;
    }

    public function provinces()
    {
        $provinces = $this->provinceContract->all();

        return $provinces;
    }
}
