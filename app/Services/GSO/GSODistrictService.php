<?php

namespace App\Services\GSO;

use App\Repositories\GSODistrictContract;

/**
 *
 */
class GSODistrictService extends \App\Services\BaseService
{
    /**
     * @var GSODistrictContract
     */
    protected $districtContract;

    /**
     * @param GSODistrictContract $districtContract
     */
    public function __construct(GSODistrictContract $districtContract)
    {
        $this->districtContract = $districtContract;
    }

    public function getListDistrictsInProvince($province_id)
    {
        $districts = $this->districtContract->districtsInProvince($province_id);

        return $districts;
    }
}
