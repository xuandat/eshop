<?php

namespace App\Services;

use App\Models\Role;
use App\Repositories\Eloquents\RoleRepository;
use App\Repositories\Eloquents\UserRepository;
use App\Repositories\RoleRepositoryContract;
use Illuminate\Http\Request;

class RoleService extends  BaseService
{

    /**
     * repository
     *
     * @var RoleRepository
     */
    protected $repository;

    public function __construct(RoleRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    public function fetchData(Request $request)
    {
        $perPage = $request->input('per_page');
        $search = $request->input('search') ?? null;

        $resources = $this->repository->fetchData($perPage, $search);

        return $resources;
    }

    public function fetchById($id)
    {
        $role = $this->repository->getById($id);

        return $role;
    }


    /**
     * store
     *
     * @param  Request $request
     * @return Role
     */
    public function store(Request $request)
    {
        $role = parent::store($request);
        $role->permissions()->attach($request->permission_id);

        return $role;
    }

    /**
     * Update
     *
     * @param string|int $id
     * @param Request $request
     *
     * @return Role
     */
    public function update($id, Request $request)
    {
        $role = parent::update($id, $request);
        $role->permissions()->sync($request->input('permission_id'));

        return $role;
    }
}
