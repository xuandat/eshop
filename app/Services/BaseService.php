<?php

namespace App\Services;

use App\Repositories\Eloquents\BaseRepository;
use BaconQrCode\Common\Mode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

abstract class BaseService
{

    /**
     * @var BaseRepository
     */
    protected $repository;

    public function list(Request $request)
    {
        $perPage = $request->input('per_page') ? (int)$request->input('per_page') : 5;
        $models = $this->repository->paginate($perPage);

        return $models;
    }

    public function destroy($id): int
    {
        return $this->repository->destroyMultipleById([$id]);
    }

    public function show($id): ?Model
    {
        return $this->repository->getById($id);
    }

    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    public function update($id, Request $request)
    {
        $resourceUpdate = $request->except('password');

        $resourceCurrent = $this->repository->getById($id)->toArray();
        $resourceUpdate = array_diff_assoc($resourceUpdate, $resourceCurrent);

        return $this->repository->updateById($id, $resourceUpdate);
    }
}
