<?php

namespace App\Services;

use App\Models\Role;
use App\Repositories\Eloquents\PermissionRepository;
use App\Repositories\Eloquents\RoleRepository;
use App\Repositories\Eloquents\UserRepository;
use App\Repositories\PermissionRepositoryContract;
use App\Repositories\ServiceRepositoryContract;
use Illuminate\Http\Request;

class PermissionService extends  BaseService
{
    /**
     * permission
     *
     * @var PermissionRepository
     */
    protected $permission;

    public function __construct(PermissionRepositoryContract $permission)
    {
        $this->permission = $permission;
    }

    public function fetchData(Request $request)
    {
        $perPage = $request->input('per_page');
        $search = $request->input('search') ?? null;

        $resources = $this->permission->fetchData($perPage, $search);

        return $resources;
    }

    public function fetchPermissionParents()
    {
        $permissions = $this->permission->allPermissionParents();

        return $permissions;
    }

    public function fetchAll()
    {
        $resources = $this->permission->getAll();

        return $resources;
    }
}
