<?php

/** @noinspection ALL */

namespace App\Services;

use App\Repositories\Eloquents\ProductRepository;
use App\Repositories\ProductRepositoryInterface;
use App\Traits\FileUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductService extends BaseService
{
    use FileUpload;

    /**
     * productRepository
     *
     * @var ProductRepository
     */
    protected $repository;

    /**
     * __construct
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->repository = $productRepository;
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['user_id'] = auth()->id();
        $data['images'] = $this->uploadFile($request);

        return $this->repository->create($data);
    }

    public function list(Request $request)
    {
        $keyword = $request->input('key_word_live_search');

        if (!isset($keyword) || trim($keyword) === '') {
            return parent::list($request);
        } else {
            $perPage = $request->input('per_page') ? (int)$request->input('per_page') : 5;

            return $this->repository->search($perPage, $keyword);
        }
    }
}
