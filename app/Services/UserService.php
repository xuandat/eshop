<?php

namespace App\Services;

use App\Repositories\Eloquents\UserRepository;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserService extends  BaseService
{
    /**
     * @var UserRepository
     */
    protected $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function list(Request $request)
    {
        $keyword = $request->input('key_word_live_search');

        if (!isset($keyword) || trim($keyword) === '') {
            return parent::list($request);
        } else {
            $perPage = $request->input('per_page') ? (int)$request->input('per_page') : 5;

            return $this->repository->search($perPage, $keyword);
        }
    }
}
