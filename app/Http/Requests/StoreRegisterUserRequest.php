<?php

namespace App\Http\Requests;

use App\Rules\VietnamPhoneNumberRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreRegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'last_name' => 'required',
            'email' => ['required', 'unique:users'],
            'password' => ['required', 'min:8'],
            'phone' => ['required', 'unique:users', new VietnamPhoneNumberRule],
            'province' => 'required',
            'district' => 'required',
            'ward_street' => 'required',
            'address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name khong duoc bo trong',
            'last_name.required' => 'Last Name khong duoc bo trong',
            'email.required' => 'Email khong duoc de trong',
        ];
    }
}
