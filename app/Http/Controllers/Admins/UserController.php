<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRegisterUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Customer;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class UserController extends Controller
{
    /**
     *
     * @var UserService
     */
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Get listing of resource
     */
    public function list(Request $request)
    {
        $resources = $this->userService->list($request);

        return response()->json([
            'status' => Response::$statusTexts[200],
            'status_code' => 200,
            'data' => $resources,
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.customers.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRegisterUserRequest $request)
    {
        $resource = $this->userService->store($request);

        return response()->json([
            'status' => Response::$statusTexts[200],
            'status_code' => 200,
            'resource' => $resource,
            'message' => 'Create Success',
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resource = $this->userService->show($id);

        if ($resource) {
            return response()->json([
                'status' => Response::$statusTexts[200],
                'status_code' => 200,
                'data' => $resource,
            ], Response::HTTP_OK);
        }

        return response()->json([
            'status' => Response::$statusTexts[404],
            'status_code' => 404,
            'errors' => 'Not Found',
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $resource = $this->userService->update($id, $request);

        return response()->json([
            'status' => Response::$statusTexts[200],
            'status_code' => 200,
            'resource' => $resource,
            'message' => 'Update Success',
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->userService->destroy($id);

        if ($result === 1) {
            return response()->json([
                'status' => Response::$statusTexts[200],
                'status_code' => 200,
                'message' => 'Delete Success',
            ], Response::HTTP_OK);
        }

        return response()->json([
            'status' => Response::$statusTexts[500],
            'status_code' => 500,
            'errors' => 'Delete Fail',
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function register(Request $request)
    {
        return 'register';
    }
}
