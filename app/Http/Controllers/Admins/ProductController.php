<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.products.index');
    }

    /**
     * return a listing of the resource
     */
    public function list(Request $request)
    {
        $products = $this->productService->list($request);

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[200],
            'status_code' => 200,
            'data' => $products,
        ], HttpFoundationResponse::HTTP_OK);
    }

    #region Destroy
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);

        $result = $this->productService->destroy($product->id);

        if ($result === 1) {
            return response()->json([
                'status' => HttpFoundationResponse::$statusTexts[200],
                'status_code' => 200,
                'message' => 'Delete Success',
            ], HttpFoundationResponse::HTTP_OK);
        }

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[500],
            'status_code' => 500,
            'errors' => 'Delete Fail',
        ], HttpFoundationResponse::HTTP_INTERNAL_SERVER_ERROR);
    }
    #endregion

    #region Show
    public function show(Product $product)
    {
        $this->authorize('view', $product);

        $resource = $this->productService->show($product->id);

        if ($resource) {
            return response()->json([
                'status' => HttpFoundationResponse::$statusTexts[200],
                'status_code' => 200,
                'data' => $resource,
            ], HttpFoundationResponse::HTTP_OK);
        }

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[404],
            'status_code' => 404,
            'errors' => 'Not Found',
        ], HttpFoundationResponse::HTTP_NOT_FOUND);
    }
    #endregion

    public function store(StoreProductRequest $request)
    {
        $product = $this->productService->store($request);

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[200],
            'status_code' => 200,
            'product' => $product,
            'message'=>'Create Success',
        ], HttpFoundationResponse::HTTP_OK);
    }

    public function update(Request $request, Product $product)
    {
        $this->authorize('update', $product);
        $product = $this->productService->update($product->id, $request);

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[200],
            'status_code' => 200,
            'product' => $product,
            'message'=>'Update Success',
        ], HttpFoundationResponse::HTTP_OK);
    }
}
