<?php

namespace App\Http\Controllers;

use App\Services\GSO\GSODistrictService;
use App\Services\GSO\GSOProvinceService;
use App\Services\GSO\GSOWardService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class GSOController extends Controller
{
    /**
     * @var GSOProvinceService
     */
    protected $province;
    /**
     * @var GSODistrictService
     */
    protected $district;
    /**
     * @var GSOWardService
     */
    protected $ward;

    public function __construct(GSOProvinceService $province, GSODistrictService $district, GSOWardService $ward)
    {
        $this->province = $province;
        $this->district = $district;
        $this->ward = $ward;
    }

    public function indexProvince(){
        $provinces = $this->province->provinces();

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[200],
            'status_code' => 200,
            'provinces' => $provinces,
        ], HttpFoundationResponse::HTTP_OK);
    }

    public function indexDistricts($province_id)
    {
        $districts = $this->district->getListDistrictsInProvince($province_id);

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[200],
            'status_code' => 200,
            'districts' => $districts,
        ], HttpFoundationResponse::HTTP_OK);
    }

    public function indexWards($district_id)
    {
        $wards =$this->ward->getListWardsInDistrict($district_id);

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[200],
            'status_code' => 200,
            'wards' => $wards,
        ], HttpFoundationResponse::HTTP_OK);
    }
}
