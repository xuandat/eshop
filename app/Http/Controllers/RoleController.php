<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class RoleController extends Controller
{

    /**
     * role
     *
     * @var RoleService
     */
    protected $role;

    /**
     * permission
     *
     * @var PermissionService
     */
    protected $permission;

    public function __construct(RoleService $role, PermissionService $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = $this->role->fetchData($request);
        $permission_parents = $this->permission->fetchPermissionParents();

        return view('admin.roles.index')->with([
            'roles' => $roles,
            'permission_parents' => $permission_parents,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = $this->role->store($request);

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[200],
            'status_code' => 200,
            'role' => $role,
            'message' => 'Create Success',
        ], HttpFoundationResponse::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $role = $this->role->fetchById($role->id);
        $permissionsChecked = $role->permissions;
        $permissions = $this->permission->fetchAll();
        if ($role) {
            return response()->json([
                'status' => HttpFoundationResponse::$statusTexts[200],
                'status_code' => 200,
                'role' => $role,
                'permissions' => $permissions,
                'permissionsChecked'=>$permissionsChecked,
            ], HttpFoundationResponse::HTTP_OK);
        }

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[404],
            'status_code' => 404,
            'errors' => 'Not Found',
        ], HttpFoundationResponse::HTTP_NOT_FOUND);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $resource = $this->role->update($role->id, $request);

        return response()->json([
            'status' => HttpFoundationResponse::$statusTexts[200],
            'status_code' => 200,
            'resource' => $resource,
            'message' => 'Update Success',
        ], HttpFoundationResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
}
