<?php

namespace App\Http\Controllers;

use App\Models\vai_tro;
use Illuminate\Http\Request;

class VaiTroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\vai_tro  $vai_tro
     * @return \Illuminate\Http\Response
     */
    public function show(vai_tro $vai_tro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\vai_tro  $vai_tro
     * @return \Illuminate\Http\Response
     */
    public function edit(vai_tro $vai_tro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\vai_tro  $vai_tro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vai_tro $vai_tro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\vai_tro  $vai_tro
     * @return \Illuminate\Http\Response
     */
    public function destroy(vai_tro $vai_tro)
    {
        //
    }
}
