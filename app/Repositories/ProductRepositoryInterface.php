<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

interface ProductRepositoryInterface
{
    public function search($perPage, $keyWord, $columns = ['*']);
}
