<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

interface GSOWardContract
{
    public function wardsInDistrict($district_id);
}
