<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface BaseRepositoryInterface
{
    public function all();

    public function count();

    public function deleteById($id);

    public function first();

    public function get();

    public function getById($id);

    public function limit($limit);

    public function orderBy($column, $value);

    public function paginate($perPage = 15, $columns = ['*']);

    public function where($column, $value, $operator = '=');

    public function whereIn($column, $value);

    public function with($relations);

    public function create(array $data);

    public function createMultiple(array $data);

    public function delete();

    public function destroyMultipleById(array $ids): int;

    public function updateById($id, array $data);

}
