<?php

namespace App\Repositories\Eloquents;

use App\Models\Permission;
use App\Repositories\PermissionRepositoryContract;
use App\Repositories\RoleRepositoryContract;

class PermissionRepository extends BaseRepository implements PermissionRepositoryContract
{
    public function __construct(Permission $permission)
    {
        $this->model = $permission;
    }

    public function fetchData($perPage, $keyWord, $columns = ['*'])
    {
        $keyWord = str_replace(" ", "%", $keyWord);

        return $this->model
            ->oldest()
            ->paginate($perPage, $columns);
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function allPermissionParents()
    {
        $permissions = $this->model->isPermissionParents()->get();

        return $permissions;
    }
}
