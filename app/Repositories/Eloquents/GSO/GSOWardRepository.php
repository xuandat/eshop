<?php

namespace App\Repositories\Eloquents\GSO;

use App\Models\GSO\Ward;
use App\Repositories\Eloquents\BaseRepository;
use App\Repositories\GSOWardContract;
use Illuminate\Support\Collection;

class GSOWardRepository extends BaseRepository implements GSOWardContract
{
    public function __construct(Ward $ward)
    {
        $this->model = $ward;
    }

    public function wardsInDistrict($district_id)
    {
        $disstricts = $this->model->where('district_id', $district_id)->get();

        return $disstricts;
    }
}
