<?php

namespace App\Repositories\Eloquents\GSO;

use App\Models\GSO\District;
use App\Repositories\Eloquents\BaseRepository;
use App\Repositories\GSODistrictContract;
use Illuminate\Support\Collection;

class GSODistrictRepository extends BaseRepository implements GSODistrictContract
{
    public function __construct(District $district)
    {
        $this->model = $district;
    }

    public function districtsInProvince($province_id)
    {
        $districts = $this->model->where('province_id', $province_id)->get();

        return $districts;
    }
}
