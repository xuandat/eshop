<?php

namespace App\Repositories\Eloquents\GSO;

use App\Models\GSO\Province;
use App\Repositories\Eloquents\BaseRepository;
use App\Repositories\GSOProvinceContract;
use Illuminate\Support\Collection;

class GSOProvinceRepository extends BaseRepository implements GSOProvinceContract
{
    public function __construct(Province $province)
    {
        $this->model = $province;
    }
}
