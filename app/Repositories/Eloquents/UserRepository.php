<?php

namespace App\Repositories\Eloquents;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function search($perPage, $keyWord, $columns = ['*'])
    {
        $keyWord = str_replace(" ", "%", $keyWord);

        return $this->model
            ->orName($keyWord)
            ->orEmail($keyWord)
            ->orPhone($keyWord)
            ->orAddress($keyWord)
            ->latest('id')
            ->active(1)
            ->paginate($perPage, $columns);
    }
}
