<?php

namespace App\Repositories\Eloquents;

use App\Models\Role;
use App\Repositories\RoleRepositoryContract;

class RoleRepository extends BaseRepository implements RoleRepositoryContract
{
    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    public function fetchData($perPage, $keyWord, $columns = ['*'])
    {
        $keyWord = str_replace(" ", "%", $keyWord);

        return $this->model
            ->oldest()
            ->paginate($perPage, $columns);
    }
}
