<?php

namespace App\Repositories\Eloquents;

use App\Models\Product;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Support\Collection;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function search($perPage, $keyWord, $columns = ['*'])
    {
        $keyWord = str_replace(" ", "%", $keyWord);

        return $this->model
            ->orName($keyWord)
            ->orDescription($keyWord)
            ->latest('id')
            // ->active(1)
            ->paginate($perPage, $columns);
    }
}
