<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

interface GSODistrictContract
{
    public function districtsInProvince($province_id);
}
