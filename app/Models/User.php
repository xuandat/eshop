<?php

namespace App\Models;

use App\Traits\HasPermissions;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasPermissions;

    #region properties
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'phone',
        'email',
        'password',
        'address',
        'ward_id',
        'district_id',
        'province_id',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
        'full_address',
    ];

    protected $with = ['permissions', 'roles'];
    #endregion

    #region Order, Product
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'id', 'user_id');
    }
    #endregion

    #region accessor, mutator
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getFullAddressAttribute()
    {
        return "{$this->address}, {$this->ward_id}, {$this->district_id}, {$this->province_id}.";
    }
    #endregion

    #region Scope
    public function scopeCreatedDesc($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    public function scopeActive($query, $statusCode = 1)
    {
        return $query->where('status', $statusCode);
    }

    public function scopeOrName($query, $key_word)
    {
        return $query->orWhere('name', 'like', '%' . $key_word . '%')->orWhere('last_name', 'like', '%' . $key_word . '%');
    }

    public function scopeOrEmail($query, $key_word)
    {
        return $query->orWhere('email', 'like', '%' . $key_word . '%');
    }

    public function scopeOrAddress($query, $key_word)
    {
        return $query->orWhere('address', 'like', '%' . $key_word . '%');
    }

    public function scopeOrPhone($query, $key_word)
    {
        return $query->orWhere('phone', 'like', '%' . $key_word . '%');
    }
    #endregion

    #region Roles, Permission Relationship
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * hasRole
     *
     * @param mixed $role
     * @return void
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function isSuperAdmin()
    {
        return $this->roles->where('name', 'admin')->count() === 1;
    }

    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }
    #endregion

    # eager loading
    public function scopeWithPermission()
    {
    }

    #endregion
}
