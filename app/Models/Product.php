<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    const STATUS_STOP_SELLING = 0;
    const STATUS_IN_STOCK = 1;
    const STATUS_SOLD_OUT = 2;

    protected $table = 'products';

    protected $fillable  = [
        'name',
        'code',
        'price',
        'weight',
        'quantity',
        'status',
        'description',
        'user_id',
        'images',
    ];

    public function scopeCreatedDesc($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    public function scopeOrName($query, $keyWord)
    {
        return $query->orWhere('name', 'like', '%' . $keyWord . '%');
    }

    public function scopeOrDescription($query, $keyWord)
    {
        return $query->orWhere('description', 'like', '%' . $keyWord . '%');
    }

    public function scopeActive($query, $statusCode = 1){
        return $query->where('status', $statusCode );
    }

    public  function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
}
