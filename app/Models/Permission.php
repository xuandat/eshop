<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $with = ['permissions'];

    #region relation ship
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'parent_id');
    }
    #endregion

    #region Scope
    public function scopeIsPermissionParents($query)
    {
        return $query->where('parent_id', 0);
    }
    #endregion
}
