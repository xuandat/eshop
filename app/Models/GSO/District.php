<?php

namespace App\Models\GSO;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class District extends Model
{
    use HasFactory;

    protected $table = 'gso_districts';

    public function wards()
    {
        return $this->hasMany(Ward::class);
    }
}
