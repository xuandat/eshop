<?php

namespace App\Models\GSO;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $table = 'gso_provinces';

    public function districts()
    {
        return $this->hasMany(District::class);
    }
}
