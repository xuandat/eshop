<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vai_tro extends Model
{
    use HasFactory;

    public function users()
    {
        return $this->belongsToMany(User::class, 'vai_tro_user', 'vai_tro_id', 'user_id');
    }
}
